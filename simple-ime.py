#!/usr/bin/env python3


# from Xlib import X, display, Xutil
# import arabic_reshaper
# from bidi.algorithm import get_display

# def map_english_to_arabic(char):
#     # Add your mapping logic here

#     mapping = {
#         'a': 'ا',
#         'b': 'ب',
#         'c': 'ث',
#         'd': 'د',
#         'e': 'ع',
#         'f': 'ف',
#         'g': 'غ',
#         'h': 'ح',
#         'i': 'ي',
#         'j': 'ج',
#         # Add more mappings as needed
#     }

#     # Return the mapped Arabic character or the original English character if not found
#     return mapping.get(char, char)
#     # return char

# def main():
#     d = display.Display()
#     root = d.screen().root
#     root.change_attributes(event_mask=X.KeyPressMask)

#     while True:
#         event = root.display.next_event()
#         if event.type == X.KeyPress:
#             keysym = d.keycode_to_keysym(event.detail, 0)
#             if 32 <= keysym <= 126:  # English character range
#                 arabic_char = map_english_to_arabic(chr(keysym))
#                 reshaped_arabic = arabic_reshaper.reshape(arabic_char)
#                 bidi_text = get_display(reshaped_arabic)
#                 print(bidi_text)

# if __name__ == "__main__":
#     main()



# def map_english_to_arabic(char):
#     # Example mapping, you should customize this based on your needs
#     mapping = {
#         'a': 'ا',
#         'b': 'ب',
#         'c': 'ث',
#         'd': 'د',
#         'e': 'ع',
#         'f': 'ف',
#         'g': 'غ',
#         'h': 'ح',
#         'i': 'ي',
#         'j': 'ج',
#         # Add more mappings as needed
#     }

#     # Return the mapped Arabic character or the original English character if not found
#     return mapping.get(char, char)

# # Example usage:
# english_char = 'b'
# arabic_char = map_english_to_arabic(english_char)
# print(f'{english_char} maps to {arabic_char}')


# def map_english_to_arabic(char):
#     # Example mapping, you should customize this based on your needs
#     mapping = {
#         'a': 'ا',
#         'b': 'ب',
#         'c': 'ث',
#         'd': 'د',
#         'e': 'ع',
#         'f': 'ف',
#         'g': 'غ',
#         'h': 'ح',
#         'i': 'ي',
#         'j': 'ج',
#         # Add more mappings as needed
#     }

#     # Return the mapped Arabic character or the original English character if not found
#     return mapping.get(char, char)

# def main():
#     while True:
#         english_char = input("Enter an English character (or 'exit' to quit): ")

#         if english_char.lower() == 'exit':
#             break

#         arabic_char = map_english_to_arabic(english_char)
#         print(f'{english_char} maps to {arabic_char}')

# if __name__ == "__main__":
#     main()


# from pynput import keyboard
# import sys

# # Mapping English letters to Arabic letters
# letter_mapping = {
#     'a': 'ا',
#     'b': 'ب',
#     'c': 'س',
#     'd': 'د',
#     'e': 'ع',
#     # Add more mappings as needed
# }

# current_text = ''

# def on_press(key):
#     global current_text

#     try:
#         # Get the pressed key
#         char = key.char
#     except AttributeError:
#         # Handle special keys (e.g., Shift, Ctrl, etc.)
#         return

#     if char in letter_mapping:
#         # Convert the English letter to its Arabic equivalent
#         arabic_letter = letter_mapping[char]
#         current_text += arabic_letter

#         # Print the current text to the console (replace with actual output to the text area)
#         sys.stdout.write(arabic_letter)
#         sys.stdout.flush()

# def on_release(key):
#     global current_text

#     if key == keyboard.Key.esc:
#         # Stop listener when the ESC key is pressed
#         return False

# with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
#     print("IME started. Press ESC to exit.")
#     listener.join()




import getch

# English to Arabic letters mapping dictionary
english_to_arabic_mapping = {
    'a': 'ا',
    'b': 'ب',
    'c': 'س',
    'd': 'د',
    'e': 'ع',
    'f': 'ف',
    'g': 'ج',
    'h': 'ه',
    'i': 'ي',
    'j': 'ج',
    'k': 'ك',
    'l': 'ل',
    'm': 'م',
    'n': 'ن',
    'o': 'أ',
    'p': 'ب',
    'q': 'ق',
    'r': 'ر',
    's': 'س',
    't': 'ت',
    'u': 'أ',
    'v': 'ف',
    'w': 'و',
    'x': 'إكس',
    'y': 'ي',
    'z': 'ز',
}

def convert_to_arabic(input_char):
    # Convert the character to lowercase to handle both cases
    char = input_char.lower()

    # Check if the character is in the mapping dictionary
    if char in english_to_arabic_mapping:
        return english_to_arabic_mapping[char]
    else:
        # If the character is not in the mapping, keep it unchanged
        return input_char

def main():
    print("Enter English letters (press 'x' to exit):")

    while True:
        # Get a single character from the user without waiting for the Enter key
        user_input = getch.getch()

        # Check if the user wants to exit
        if user_input.lower() == 'x':
            break

        # Convert the input to Arabic and print the result
        arabic_equivalent = convert_to_arabic(user_input)
        print(f"Arabic Equivalent: {arabic_equivalent}")

if __name__ == "__main__":
    main()
