#!/usr/bin/env python3

from tkinter import *
from tkinter import ttk

def a(*args):
    return


def on_button_click(letter):
    # current_text = text.get()
    # text.set(current_text + letter)
    # return text
    return


root = Tk()
# root.title("Feet to Meters")

mainframe = ttk.Frame(root, padding = "3 3 12 12")
# # mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
mainframe.grid(column=0, row=0, sticky=(NSEW)) # NSEW : north south east west

# اُكتب_هنا = StringVar()
text = StringVar()
arabic_text = ttk.Entry(mainframe, width=100, textvariable=text).grid(column=0, row=0, columnspan=15, sticky=N)
# arabic_text = ttk.Entry(mainframe, width=100, textvariable="اُكتب هنا")

# ا ب ت ث ج ح خ د ذ ر ز س ش
# a = 1/0

ttk.Label(mainframe, text="s'").grid(column=0, row=2, sticky=N)
ttk.Button(mainframe, text="ش", width=4, command=on_button_click).grid(column=0, row=3, sticky=N)
ttk.Label(mainframe, text="s").grid(column=1, row=2, sticky=N)
ttk.Button(mainframe, text="س", width=4, command=on_button_click).grid(column=1, row=3, sticky=N)
ttk.Label(mainframe, text="z").grid(column=2, row=2, sticky=N)
ttk.Button(mainframe, text="ز", width=4, command=a).grid(column=2, row=3, sticky=N)
ttk.Label(mainframe, text="r").grid(column=3, row=2, sticky=N)
ttk.Button(mainframe, text="ر", width=4, command=a).grid(column=3, row=3, sticky=N)
ttk.Label(mainframe, text="d'").grid(column=4, row=2, sticky=N)
ttk.Button(mainframe, text="ذ", width=4, command=a).grid(column=4, row=3, sticky=N)
ttk.Label(mainframe, text="d").grid(column=5, row=2, sticky=N)
ttk.Button(mainframe, text="د", width=4, command=a).grid(column=5, row=3, sticky=N)
ttk.Label(mainframe, text="H'").grid(column=6, row=2, sticky=N)
ttk.Button(mainframe, text="خ", width=4, command=a).grid(column=6, row=3, sticky=N)
ttk.Label(mainframe, text="H").grid(column=7, row=2, sticky=N)
ttk.Button(mainframe, text="ح", width=4, command=a).grid(column=7, row=3, sticky=N)
ttk.Label(mainframe, text="j").grid(column=8, row=2, sticky=N)
ttk.Button(mainframe, text="ج", width=4, command=a).grid(column=8, row=3, sticky=N)
ttk.Label(mainframe, text="t'").grid(column=9, row=2, sticky=N)
ttk.Button(mainframe, text="ث", width=4, command=a).grid(column=9, row=3, sticky=N)
ttk.Label(mainframe, text="t").grid(column=10, row=2, sticky=N)
ttk.Button(mainframe, text="ت", width=4, command=a).grid(column=10, row=3, sticky=N)
ttk.Label(mainframe, text="b").grid(column=11, row=2, sticky=N)
ttk.Button(mainframe, text="ب", width=4, command=a).grid(column=11, row=3, sticky=N)
ttk.Label(mainframe, text="a").grid(column=12, row=2, sticky=N)
ttk.Button(mainframe, text="ا", width=4, command=a).grid(column=12, row=3, sticky=N)


#ص ض ط ظ ع غ ف ق ك ل م ن ه و ي ء
ttk.Label(mainframe, text="-").grid(column=0, row=5, sticky=N)
ttk.Button(mainframe, text="ء", width=4, command=a).grid(column=0, row=6, sticky=N)
ttk.Label(mainframe, text="y").grid(column=1, row=5, sticky=N)
ttk.Button(mainframe, text="ي", width=4, command=a).grid(column=1, row=6, sticky=N)
ttk.Label(mainframe, text="w").grid(column=2, row=5, sticky=N)
ttk.Button(mainframe, text="و", width=4, command=a).grid(column=2, row=6, sticky=N)
ttk.Label(mainframe, text="h").grid(column=3, row=5, sticky=N)
ttk.Button(mainframe, text="ه", width=4, command=a).grid(column=3, row=6, sticky=N)
ttk.Label(mainframe, text="n").grid(column=4, row=5, sticky=N)
ttk.Button(mainframe, text="ن", width=4, command=a).grid(column=4, row=6, sticky=N)
ttk.Label(mainframe, text="m").grid(column=5, row=5, sticky=N)
ttk.Button(mainframe, text="م", width=4, command=a).grid(column=5, row=6, sticky=N)
ttk.Label(mainframe, text="l").grid(column=6, row=5, sticky=N)
ttk.Button(mainframe, text="ل", width=4, command=a).grid(column=6, row=6, sticky=N)
ttk.Label(mainframe, text="k").grid(column=7, row=5, sticky=N)
ttk.Button(mainframe, text="ك", width=4, command=a).grid(column=7, row=6, sticky=N)
ttk.Label(mainframe, text="q").grid(column=8, row=5, sticky=N)
ttk.Button(mainframe, text="ق", width=4, command=a).grid(column=8, row=6, sticky=N)
ttk.Label(mainframe, text="f").grid(column=9, row=5, sticky=N)
ttk.Button(mainframe, text="ف", width=4, command=a).grid(column=9, row=6, sticky=N)
ttk.Label(mainframe, text="g'").grid(column=10, row=5, sticky=N)
ttk.Button(mainframe, text="غ", width=4, command=a).grid(column=10, row=6, sticky=N)
ttk.Label(mainframe, text="g").grid(column=11, row=5, sticky=N)
ttk.Button(mainframe, text="ع", width=4, command=a).grid(column=11, row=6, sticky=N)
ttk.Label(mainframe, text="Z").grid(column=12, row=5, sticky=N)
ttk.Button(mainframe, text="ظ", width=4, command=a).grid(column=12, row=6, sticky=N)
ttk.Label(mainframe, text="T").grid(column=13, row=5, sticky=N)
ttk.Button(mainframe, text="ط", width=4, command=a).grid(column=13, row=6, sticky=N)
ttk.Label(mainframe, text="D").grid(column=14, row=5, sticky=N)
ttk.Button(mainframe, text="ض", width=4, command=a).grid(column=14, row=6, sticky=N)
ttk.Label(mainframe, text="S").grid(column=15, row=5, sticky=N)
ttk.Button(mainframe, text="ص", width=4, command=a).grid(column=15, row=6, sticky=N)


ttk.Label(mainframe, text="Y").grid(column=0, row=8, sticky=N)
ttk.Button(mainframe, text="ى", width=4, command=a).grid(column=0, row=9, sticky=N)
ttk.Label(mainframe, text="y--").grid(column=1, row=8, sticky=N)
ttk.Button(mainframe, text="ئ", width=4, command=a).grid(column=1, row=9, sticky=N)
ttk.Label(mainframe, text="w--").grid(column=2, row=8, sticky=N)
ttk.Button(mainframe, text="ؤ", width=4, command=a).grid(column=2, row=9, sticky=N)
ttk.Label(mainframe, text="h'").grid(column=3, row=8, sticky=N)
ttk.Button(mainframe, text="ة", width=4, command=a).grid(column=3, row=9, sticky=N)
ttk.Label(mainframe, text="a--").grid(column=4, row=8, sticky=N)
ttk.Button(mainframe, text="إ", width=4, command=a).grid(column=4, row=9, sticky=N)
ttk.Label(mainframe, text="-a").grid(column=5, row=8, sticky=N)
ttk.Button(mainframe, text="أ", width=4, command=a).grid(column=5, row=9, sticky=N)
ttk.Label(mainframe, text="").grid(column=6, row=8, sticky=N)
ttk.Button(mainframe, text="ٱ", width=4, command=a).grid(column=6, row=9, sticky=N)
ttk.Label(mainframe, text="aa").grid(column=7, row=8, sticky=N)
ttk.Button(mainframe, text="آ", width=4, command=a).grid(column=7, row=9, sticky=N)
# some more button with hover functionality



# ـ . ، ؛ ! ؟
ttk.Button(mainframe, text="؟", width=4, command=a).grid(column=0, row=12, sticky=N)
ttk.Button(mainframe, text="!", width=4, command=a).grid(column=1, row=12, sticky=N)
ttk.Button(mainframe, text="؛ ", width=4, command=a).grid(column=2, row=12, sticky=N)
ttk.Button(mainframe, text="،", width=4, command=a).grid(column=3, row=12, sticky=N)
ttk.Button(mainframe, text=".", width=4, command=a).grid(column=4, row=12, sticky=N)
ttk.Button(mainframe, text="ـ", width=4, command=a).grid(column=5, row=12, sticky=N)












# ٪ ٫ ٬ ٩ ٨ ٧ ٦ ٥ ٤ ٣ ٢ ١ ٠





root.mainloop()
